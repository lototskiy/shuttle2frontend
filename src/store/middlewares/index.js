import spinnerMiddleware from './spinner.middleware';

const middlewares = [
    spinnerMiddleware,
];

export default middlewares;
