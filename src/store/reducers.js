import { combineReducers } from 'redux';
import locationReducer from './location';

const makeRootReducer = asyncReducers => combineReducers({
    location: locationReducer,
    ...asyncReducers,
});

export const injectReducer = (store, { key, reducer }) => {
    if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

    store.asyncReducers[key] = reducer;// eslint-disable-line
    store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
