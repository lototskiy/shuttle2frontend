import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { browserHistory } from 'react-router';
import createLogger from 'redux-logger';

import { makeRootReducer } from './reducers';

import { updateLocation } from './location';

// Custom Middlewares
import middlewares from './middlewares';

const reducers = require('./reducers').default;

export default (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
    const middleware = [
        thunk,
        ...middlewares,
          // routerMiddleware(browserHistory),
    ];
    if (process.env.NODE_ENV === 'development') {
        const logger = createLogger({
              // collapsed: (getState, action) => action.type === '@@router/LOCATION_CHANGE',
            predicate: (getState, action) => action.type !== '@@router/LOCATION_CHANGE',
        });
        middleware.push(logger);
    }

  // ======================================================
  // Store Enhancers
  // ======================================================
    const enhancers = [];
    if (__DEV__) {
        const devToolsExtension = window.devToolsExtension;
        if (typeof devToolsExtension === 'function') {
            enhancers.push(devToolsExtension());
        }
    }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
    const store = createStore(
    makeRootReducer(),
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers,
    ),
  );
    store.asyncReducers = {};

  // To unsubscribe, invoke `store.unsubscribeHistory()` anytime
    store.unsubscribeHistory = browserHistory.listen(updateLocation(store));

    if (module.hot) {
        module.hot.accept('./reducers', () => {
            store.replaceReducer(reducers(store.asyncReducers));
        });
    }

    return store;
};
