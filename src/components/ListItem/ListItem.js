/** Example of usage:
*   <ListItem
        text='Contacts'
        href='contacts.html'
        linkClass='main-menu__item'
    />
*/

import React, { PropTypes } from 'react';

import './ListItem.css';

/**
*  component for list item
*/

const ListItem = (props) => {
    /**
    *  property 'imgSrc' defines list item rendering
    */
    let li;
    if (props.imgSrc) {
        li = (
        <li>
            <a href={props.href} className={props.linkClass}>
                <img src={props.imgSrc} alt={props.imgName} />{props.text}
            </a>
        </li>
        );
    } else {
        li = (
        <li>
            <a href={props.href} className={props.linkClass}>{props.text}</a>
        </li>
        );
    }
    return li;
};

ListItem.propTypes = {
    text: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    linkClass: PropTypes.string,
    imgSrc: PropTypes.string,
    imgName: PropTypes.string,
};

export default ListItem;
