/** Example of usage:
*   <NavHeader
      index
    />
*/

import React, { PropTypes } from 'react';

import './NavHeader.css';

import Logo from '../../components/Logo';
import ListItem from '../../components/ListItem';

import ua from './images/ua.jpg';
import ru from './images/ru.jpg';
import eng from './images/eng.jpg';
import ger from './images/ger.jpg';

/**
*  component for header navigation
*/

const NavHeader = (props) => {
/**
*  language choice menu
*/
    const languagesList = [
    { text: ' УКР', imgSrc: ua, imgName: 'UA' },
    { text: ' РУС', imgSrc: ru, imgName: 'RU' },
    { text: ' ENG', imgSrc: eng, imgName: 'ENG' },
    { text: ' GER', imgSrc: ger, imgName: 'GER' },
    ];

    const languages = languagesList.map((lang, index) =>
        <ListItem
        key={index}
        text={lang.text}
        href='#'
        imgSrc={lang.imgSrc}
        imgName={lang.imgName}
        />);

/**
*  main menu
*  property index defines main menu rendering
*/

    let menuList;
    if (props.index) {
        menuList = [
        { text: 'Животные', href:'#pets' },
        { text: 'Отзывы', href:'#recall' },
        { text: 'Новости', href:'#news' },
        { text: 'О нас', href:'#about' },
        { text: 'Контакты', href:'html/contacts.html' },
        ];
    } else {
        menuList = [
        { text: 'Главная', href:'index.html' },
        { text: 'Отзывы', href:'html/recall.html' },
        { text: 'Новости', href:'html/news.html' },
        { text: 'Контакты', href:'html/contacts.html' },
        ];
    }

    const mainMenu = menuList.map((menuItem, index) =>
        <ListItem
        key={index}
        text={menuItem.text}
        href={menuItem.href}
        linkClass='main-menu__item'
        />);

    return (
        <nav className='navbar-default  nav-pets' role='navigation'>
        <div className='container  main-menu'>
            <div className='navbar-header'>
                <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='#top_menu'>
                    <span className='sr-only'>Toggle navigation</span>
                    <span className='icon-bar' />
                    <span className='icon-bar' />
                    <span className='icon-bar' />
                </button>
                <Logo
                logoClass='navbar-brand  main-menu__logo'
                />
            </div>

            <div className='collapse navbar-collapse navbar-right' id='top_menu'>
                <ul className='nav navbar-nav navbar-right hidden-xs'>
                    <li className='dropdown'>
                        {/* eslint-disable max-len */}
                        <a href='#' className='dropdown-toggle main-menu__flag' data-toggle='dropdown'role='button' aria-haspopup='true' aria-expanded='false'>
                        {/* eslint-enable max-len */}
                            <img src={ua} alt='UA' /> УКР<span className='caret' />
                        </a>
                        <ul className='dropdown-menu'>
                            {languages}
                        </ul>
                    </li>
                </ul>

                <ul className='nav navbar-nav main-menu__list'>
                    {mainMenu}
                </ul>
            </div>
        </div>
    </nav>
    );
};

NavHeader.propTypes = {
    index: PropTypes.bool,
};

export default NavHeader;
