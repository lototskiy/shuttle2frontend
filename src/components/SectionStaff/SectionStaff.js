/** Example of usage:
*   <SectionStaff
      classRow='col-lg-6 col-xs-12'
    />
*/

import React, { PropTypes } from 'react';

import './SectionStaff.css';

import SectionTitle from '../../components/SectionTitle';
import Figure from '../../components/Figure';

import staff1 from './images/staff01.png';
import staff2 from './images/staff02.png';
import staff3 from './images/staff03.png';
import staff4 from './images/staff04.png';

/**
*  section component for section staff
*/

const SectionStaff = props => (
    <section className='staff'>
        <div className='container'>
            <SectionTitle
            title='Staff'
            href='contacts.html'
            />

            <div className='row'>
                <div className={props.classRow}>
                    <Figure
                    imgSrc={staff1}
                    imgAlt='Photo'
                    caption='Name Name'
                    text='Lorem ipsum dolor sit amet'
                    imgClass='staff__image--small'
                    />
                </div>

                <div className={props.classRow}>
                    <Figure
                    imgSrc={staff2}
                    imgAlt='Photo'
                    caption='Name Name'
                    text='Lorem ipsum dolor sit amet'
                    imgClass='staff__image--small'
                    />
                </div>

                <div className={props.classRow}>
                    <Figure
                    imgSrc={staff3}
                    imgAlt='Photo'
                    caption='Name Name'
                    text='Lorem ipsum dolor sit amet'
                    imgClass='staff__image--small'
                    />
                </div>

                <div className={props.classRow}>
                    <Figure
                    imgSrc={staff4}
                    imgAlt='Photo'
                    caption='Name Name'
                    text='Lorem ipsum dolor sit amet'
                    imgClass='staff__image--small'
                    />
                </div>

            </div>
        </div>
    </section>
);

SectionStaff.propTypes = {
    classRow: PropTypes.string.isRequired,
};

export default SectionStaff;
