/**
 * ContactPhones component
 * Created by irina.zvarych.
 * Example of usage:
 * <ContactPhones
      FooterBox='footer'
      PhoneNumbers={['phone', 'phone1', 'phone2']}
      TitleText='Наши телефоны'
    />
 */
import React, { PropTypes } from 'react';
import './ContactPhonesComponent.css';
import './FooterPhones.css';

/**
 * ContactPhones component
 *
 * @param {any} props
 * @returns ContactPhones
 */

const ContactPhones = (props) => {
    let PhoneBox;
    if (props.FooterBox === 'footer') {
        PhoneBox = (
            <div>
                <i className='fa fa-phone footer__icon' aria-hidden='true' />
                {props.PhoneNumbers.map(element => <p>{element}</p>)}
            </div>
        );
    } else {
        PhoneBox = (
            <div className='col-md-4 col-sm-4 col-xs-12'>
                <h4 className='staff__name'>{props.TitleText}</h4>
                {props.PhoneNumbers.map(element => <p>{element}</p>)}
            </div>
        );
    }
    return PhoneBox;
};

ContactPhones.propTypes = {
    FooterBox:PropTypes.string,
    TitleText: PropTypes.string,
    PhoneNumbers: PropTypes.array,
};
export default ContactPhones;
