/**
 * Address component
 * Created by irina.zvarych.
 * Example of usage:
 * <Address
 *     FooterBox='footer' - if footer block
       AddressClass='address'
       IndexText='Харьков, 61001'
       StreetText='ул. Хххххххххх, 77'
       OfficeText='3 этаж, оф. 317'
    />
 */
import React, { PropTypes } from 'react';
import './Address.css';
import './FooterAddress.css';

/**
 * Address component
 *
 * @param {any} props
 * @returns Address
 */
const Address = (props) => {
    let AddressBox;
    if (props.FooterBox === 'footer') {
        AddressBox = (
          <div className='hidden-md hidden-sm hidden-lg col-xs-12'>
            <br />
            <i className='fa fa-map-marker footer__icon' aria-hidden='true' />
            <address className='address'>
                  <br />
                  <strong>Служба порятнуку тварин</strong><br />
                  Украина<br />
                  {props.IndexText}<br />
                  {props.StreetText}<br />
                  {props.OfficeText}
            </address>
          </div>
        );
    } else {
        AddressBox = (
       <address className='address'>
              <br />
              <strong>Служба порятнуку тварин</strong><br />
              Украина<br />
              {props.IndexText}<br />
              {props.StreetText}<br />
              {props.OfficeText}
        </address>
        );
    }
    return AddressBox;
};

Address.propTypes = {
    FooterBox:PropTypes.string,
    IndexText: PropTypes.string,
    StreetText: PropTypes.string,
    OfficeText: PropTypes.string,
};

export default Address;
