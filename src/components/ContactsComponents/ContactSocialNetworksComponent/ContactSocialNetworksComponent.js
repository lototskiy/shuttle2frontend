/**
 * ContactSocialNetworks component
 * Created by irina.zvarych.
 * Example of usage:
 * <ContactSocialNetworks
    TitleForBox='Мы в соцсетях'
    />
 */
import React, { PropTypes } from 'react';
import './ContactSocialNetworksComponent.css';

/**
 * ContactSocialNetworks component
 *
 * @param {any} props
 * @returns ContactSocialNetworks
 */
const ContactSocialNetworks = props => (
    <div>
        <h4 className='staff__name'>{props.TitleForBox}</h4>
        <i className='fa fa-facebook-square footer__icon footer__icon--center' aria-hidden='true' />
        <i className='fa fa-twitter-square footer__icon footer__icon--center' aria-hidden='true' />
        <i classNamе='fa fa-google-plus-square footer__icon footer__icon--center' aria-hidden='true' />
    </div>
);

ContactSocialNetworks.propTypes = {
    TitleForBox: PropTypes.string,
};

export default ContactSocialNetworks;
