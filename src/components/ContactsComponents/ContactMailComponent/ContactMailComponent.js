/**
 * ContactMail component
 * Created by irina.zvarych.
 * Example of usage:
 * <ContactMail
    FooterBox='footer'
    TitleForBox='Пишите нам'
    MailAddress='savepets@spt.kh'
    />
 */
import React, { PropTypes } from 'react';
import './ContactMailComponent.css';
import './FooterEmail.css';

/**
 * ContactMail component
 *
 * @param {any} props
 * @returns ContactMail
 */
const ContactMail = (props) => {
    let MailBox;
    if (props.FooterBox === 'footer') {
        MailBox = (
            <div>
                <i className='fa fa-envelope-o footer__icon' aria-hidden='true' />
                <a href={`mailto:${props.MailAddress}`}>{props.MailAddress}</a>
            </div>
        );
    } else {
        MailBox = (
            <div>
                <h4 className='staff__name'>{props.TitleForBox}</h4>
                <a href={`mailto:${props.MailAddress}`}>{props.MailAddress}</a>
                <br />
                <br />
            </div>
        );
    }
    return MailBox;
};

ContactMail.propTypes = {
    FooterBox: PropTypes.string,
    TitleForBox: PropTypes.string,
    MailAddress: PropTypes.string,
};

export default ContactMail;
