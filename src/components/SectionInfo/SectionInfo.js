/**
 * Created by vitalii.petrenko on 12/10/16.
 */

import React, { PropTypes } from 'react';

import './SectionInfo.css';
import SectionInfoImg from './SectionInfo.png';

const SectionInfo = props => (
    <section className='info'>
        <div className='container'>
            <SectionDescription />

            <div className='row'>
                <div className='col-md-12 col-sm-12 col-xs-12'>
                    <img src='{props.ImageUrl}' alt='slider' className='slider' />
                </div>
            </div>

            <div className='row'>
                <div className='col-md-6 col-md-offset-3'>
                    <p className='info__text'>{props.InfoText}</p>
                </div>
            </div>
        </div>
    </section>
);

SectionInfo.propTypes = {
    ImageUrl: PropTypes.string,
    InfoText: PropTypes.string,
};

export default SectionInfo;
