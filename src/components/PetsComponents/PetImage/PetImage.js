/**
 * Created by Andrii Kryshchuk on 12/11/16.
 *   <PetImage
 *      PetName='Bethoven'
 *      PetImg='http://polezno.tv/wp-content/uploads/2016/11/image-slider2-900x400.jpg'
 *  />
 */

import React, { PropTypes } from 'react';

import './PetImage.css';

/**
 *  form component for display the image of the Pet (with name)
 *  @param {string} PetName -name of Pet.
 *  @param {string} PetImg - src link for Pet image.
 */

const PetImage = (props) => {
    const PetImageBox = (
      <div className='pets__img'>
        <figure>
            <img src={props.PetImg} alt='' className='pets__foto' />
            <figcaption>
                <h4 className='staff__name'>{props.PetName}</h4>
            </figcaption>
        </figure>
      </div>
    );
    return PetImageBox;
};

PetImage.propTypes = {
    PetName: PropTypes.string,
    PetImg: PropTypes.string,
};

export default PetImage;
