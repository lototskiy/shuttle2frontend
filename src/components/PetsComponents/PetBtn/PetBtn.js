/**
 * Created by Andrii Kryshchuk on 14/11/16.
 *   <PetBtn
 *      PetHelpBtnName={props.PetItemObject.PetBtn.PetHelpBtnName}
 *      PetWantBtnName={props.PetItemObject.PetBtn.PetWantBtnName}
 *      PetHelpClickHandler={clickHandlerHelp}
 *      PetWantClickHandler={clickHandlerWant}
 *   />
 */

import React, { PropTypes } from 'react';

import './PetBtn.css';

/**
 *  form component for display two buttons of Pet Item
 * @param {string} PetWantBtnName - signature on the button 'Want'.
 * @param {string} PetHelpBtnName - signature on the button 'Help'.
 * @param {func} PetHelpClickHandler - function for 'Help' button functionality.
 * @param {func} PetWantClickHandler - function for 'Want' button functionality.
 */


const PetBtn = (props) => {
    const PetBtnBox = (
      <div>
        <button className='pets__btn' onClick={props.PetWantClickHandler}>{props.PetWantBtnName}</button>
        <button className='pets__btn' onClick={props.PetHelpClickHandler}>{props.PetHelpBtnName}</button>
      </div>
    );
    return PetBtnBox;
};

PetBtn.propTypes = {
    PetWantBtnName: PropTypes.string,
    PetHelpBtnName: PropTypes.string,
    PetHelpClickHandler: PropTypes.func,
    PetWantClickHandler: PropTypes.func,
};

export default PetBtn;
