/**
 * Created by Andrii Kryshchuk on 12/11/16.
 *  Example of usage:
 *   <PetName
 *      petNameClass='pets__name pets__name--invert'
 *      namePet={ name: 'Dog',
 *                breed: 'noname',
 *                age: '5',
 *                color: 'black',
 *               }
 *  />
 */

import React, { PropTypes } from 'react';

import './PetName.css';

/**
*  form component for display Name, Age, Breed and Color of the Pet (part of Pet Item)
*  @param {object} namePet - object with name, breed, age, and color of Pet.
*  @param {string} petNameClass - style of component.
*/

const PetName = (props) => {
    const PetNameBox = (
      <div className={props.petNameClass}>
              {props.namePet.name}<br /> {props.namePet.breed}
              <br /> {props.namePet.age}
              <br /> {props.namePet.color}
      </div>);
    return PetNameBox;
};

PetName.propTypes = {
    petNameClass: PropTypes.string,
    namePet: PropTypes.object,
};

export default PetName;
