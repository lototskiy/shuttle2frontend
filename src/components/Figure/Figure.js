/** Example of usage:
*   <Figure
      imgSrc={logo}
      imgAlt='logo'
      imgClass='main_logo'
      caption='Logo'
    />
*/

import React, { PropTypes } from 'react';

import './Figure.css';

/**
*  figure component for image with caption and description
*/

const Figure = (props) => {
    /**
    *  property 'href' defines image rendering
    */
    let image = null;
    if (!props.href) {
        image = (
        <img src={props.imgSrc} alt={props.imgAlt} className={props.imgClass} />
        );
    } else {
        image = (
        <a href={props.href}>
          <img src={props.imgSrc} alt={props.imgAlt} className={props.imgClass} />
        </a>
        );
    }
    return (
      <figure>
        {image}
        <figcaption>
          <h4 className='staff__name'>{props.caption}</h4>
        </figcaption>
        {(props.text) && <p className={props.textClass}>{props.text}</p>}
      </figure>
    );
};

Figure.propTypes = {
    imgSrc: PropTypes.string.isRequired,
    imgAlt: PropTypes.string.isRequired,
    imgClass: PropTypes.string.isRequired,
    caption: PropTypes.string.isRequired,
    text: PropTypes.string,
    href: PropTypes.string,
    textClass: PropTypes.string,
};

export default Figure;
