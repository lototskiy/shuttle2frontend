/** Example of usage:
*   <SectionTitle
      title='Staff'
      href='contacts.html'
    />
*/

import React, { PropTypes } from 'react';

import './SectionTitle.css';

/**
*  component for section title
*/

const SectionTitle = (props) => {
    /**
    *  property 'href' defines title rendering
    */
    let title = null;
    if (!props.href) {
        title = (
        <h2 className='staff__tittle'>{props.title}</h2>
        );
    } else {
        title = (
        <a href={props.href} className='happyend__link'>
            <h2 className='staff__tittle'>{props.title}</h2>
        </a>
        );
    }
    return (
    <div className='row'>
        <div className='col-xs-12'>
            {title}
        </div>
    </div>
    );
};

SectionTitle.propTypes = {
    title: PropTypes.string.isRequired,
    href: PropTypes.string,
};

export default SectionTitle;
