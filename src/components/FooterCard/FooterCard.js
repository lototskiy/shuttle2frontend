/**
 * FooterCard component
 * Created by irina.zvarych.
 * Example of usage:
 * <FooterCard
    NumberCard='1234 4567 1234 4566'
    />
 */
import React, { PropTypes } from 'react';
import './FooterCard.css';

/**
 * FooterCard component
 *
 * @param {any} props
 * @returns FooterCard
 */
const FooterCard = props => (
   <div><i className='fa fa-cc-mastercard footer__icon' aria-hidden='true' />
    <p>{props.NumberCard}</p></div>
);

FooterCard.propTypes = {
    NumberCard: PropTypes.string,
};

export default FooterCard;
