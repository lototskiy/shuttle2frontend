/**
 * Created by vitalii.petrenko on 12/10/16.
 */

import React, { PropTypes } from 'react';

import './Logo.css';
import logoImg from './logo2.png';

const Logo = (props) => {
    console.log('props: ', props);
    return (
      <a
        className={props.logoClass}
        href='index.html'
        onClick={props.clickHandler}
      >
        <img alt='Logo' src={logoImg} />
      </a>
    );
};

Logo.propTypes = {
    logoClass: PropTypes.string,
    clickHandler: PropTypes.func,
};

export default Logo;
