import React from 'react';
import { IndexLink, Link } from 'react-router';
import './Header.scss';

import Logo from '../../components/Logo';
import SectionInfo from '../../components/SectionInfo';

const Header = () => {
    const clickHandler = (e) => {
        e.preventDefault();
        console.log('Clicked !!!');
    };
    return (
  <div>
    <Logo
      logoClass='navbar-brand  main-menu__logo'
      clickHandler={clickHandler}
    />
    <SectionInfo
    ImageUrl='../img/slider.png'
    InfoText='Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
    />
    <IndexLink to='/' activeClassName='route--active'>
      Home
    </IndexLink>
    {' · '}
    <Link to='/counter' activeClassName='route--active'>
      Counter
    </Link>
    {' · '}
    <Link to='/example' activeClassName='route--active'>
      Example
    </Link>
  </div>
    );
};

export default Header;
