/**
 * ContactSocialNetworks component
 * Created by irina.zvarych.
 * Example of usage:
 * <ContactSocialNetworks
    TitleForBox='Мы в соцсетях'
    />
 */
import React, { PropTypes } from 'react';
import './Footer.css';

import Address from '../../components/ContactsComponents/Address';
import FooterCard from '../../components/FooterCard';
import ContactMail from '../../components/ContactsComponents/ContactMailComponent';
import ContactPhones from '../../components/ContactsComponents/ContactPhonesComponent';
import ContactSocialNetworks from '../../components/ContactsComponents/ContactSocialNetworksComponent';

/**
 * ContactSocialNetworks component
 *
 * @param {any} props
 * @returns ContactSocialNetworks
 */

const FooterComponent = props => (
    <footer className='footer'>
        <div className='container'>
            <div className='row'>
                <div className='col-md-4 col-sm-4 col-xs-6'>
                    <ContactMail
                    FooterBox='footer'
                    TitleForBox='Пишите нам'
                    MailAddress='savepets@spt.kh'
                    />
                    <ContactPhones
                    FooterBox='footer'
                    PhoneNumbers={['phone', 'phone1', 'phone2']}
                    TitleText='Наши телефоны'
                    />
                </div>

                <div className='col-md-4 col-sm-4 col-xs-6'>
                     <FooterCard
                        NumberCard='1234 4567 1234 4566'
                     />
                    <ContactSocialNetworks
                        TitleForBox='Заголовок бокса'
                    />
                </div>

                <Address
                FooterBox='footer'
                AddressClass='address'
                IndexText='Харьков, 61001'
                StreetText='ул. Хххххххххх, 77'
                OfficeText='3 этаж, оф. 317'
                />
            </div>

            <div className='row'>
                <Address
                FooterBox='footer'
                AddressClass='address'
                IndexText='Харьков, 61001'
                StreetText='ул. Хххххххххх, 77'
                OfficeText='3 этаж, оф. 317'
                />
            </div>
        </div>
    </footer>
);

export default FooterComponent;
