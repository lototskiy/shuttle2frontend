<div class="hidden-md hidden-sm hidden-lg col-xs-12">
        <br>
        <i class="fa fa-map-marker footer__icon" aria-hidden="true"></i>
        <address>
            <strong>Служба порятунку животних</strong><br>
            Украина 
            Харьков, 61052
            ул. Нарядная, 17
            оф. 213
        </address>
</div>

<!-- address after resize  
<div class="col-md-4 col-sm-4 hidden-xs">
    <i class="fa fa-map-marker footer__icon" aria-hidden="true"></i>
    <address>
        <strong>Служба порятунку<br>животних</strong><br>
        Украина <br>
        Харьков, 61052<br>
        ул. Нарядная, 17<br>
        оф. 213
    </address>
</div> 
-->