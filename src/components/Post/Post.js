/** Example of usage:
*   <Post
      postDate='01.01.2016'
      text='text'
      href='index.html'
    />
*/

import React, { PropTypes } from 'react';

import './Post.css';

/**
*  component for post
*/

const Post = props => (
    <div>
        <time>{props.postDate}</time>
        <p>{props.text}</p>
        <a href={props.href}>Подробнее...</a>
    </div>
);

Post.propTypes = {
    postDate: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
};

export default Post;
