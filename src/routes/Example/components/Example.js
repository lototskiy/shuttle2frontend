import React, { PropTypes } from 'react';

export const Example = props => (
  <div>
    <h2>Put your components here</h2>
  </div>
);

Example.propTypes = {
    counter     : PropTypes.number,
    doubleAsync : PropTypes.func,
    increment   : PropTypes.func,
};

export default Example;
